# Discussions

A place for discussions about X11-Basic. Only the issues are used in replacement for a users forum. If you want to report a bug on the X11-Basic interpreter or compiler or manual etself, please create an issue in the source code repositories:
https://codeberg.org/kollo/X11Basic or
https://codeberg.org/kollo/X11-Basic (Android version).
